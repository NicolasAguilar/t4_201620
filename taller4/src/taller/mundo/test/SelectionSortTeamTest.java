package taller.mundo.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.SelectionSortTeam;

public class SelectionSortTeamTest extends TestCase{
	
	private SelectionSortTeam sel;
	private Comparable[] guia;
	private Comparable[] list;
	
	protected void setup()
	{
		sel =  new SelectionSortTeam();
		guia =  new Comparable[]{3,21,35,45,51,62,78,80,93,99};
		list =  new Comparable[]{51,3,93,62,99,21,80,35,78,45};
	}
	
	public void testSelectionSortAscendente()
	{
		setup();
		list = sel.sort(list, TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[i]);
		}
	}
	
	public void testSelectionSortDescendente()
	{
		setup();
		list = sel.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[guia.length - 1 -i]);
		}
	}
}
