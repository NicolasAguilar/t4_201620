package taller.mundo.test;

import java.util.Random;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import junit.framework.TestCase;

public class BubbleSortTeamTest extends TestCase{

	private BubbleSortTeam bub;
	private Comparable[] guia;
	private Comparable[] list;

	protected void setup()
	{
		bub = new BubbleSortTeam();
		guia = new Comparable[]{1,6,9,10,12,17,29,30,35,40};
		list = new Comparable[]{30,6,17,9,1,40,35,12,29,10};
	}

	public void testBubbleSortAscendente()
	{
		setup();
		list = bub.sort(list, TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length;i++)
			assertEquals(list[i], guia[i]);
	}

	public void testBubbleSortDescendente()
	{
		setup();
		list = bub.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length;i++)
			assertEquals(list[i], guia[guia.length-1-i]);
	}
}
