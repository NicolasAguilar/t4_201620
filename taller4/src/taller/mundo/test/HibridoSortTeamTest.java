package taller.mundo.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.HibridoSortTeam;
import taller.mundo.teams.InsertionSortTeam;
import junit.framework.TestCase;

public class HibridoSortTeamTest extends TestCase{

	private HibridoSortTeam hib;
	private Comparable[] guia;
	private Comparable[] list;
	
	public void setup()
	{
		hib =  new HibridoSortTeam();
		guia = new Comparable[]{14,27,34,38,47,57,69,78,85,95};
		list = new Comparable[]{38,34,85,27,47,78,95,57,69,14};
	}
	
	public void testInsertionSortAscendente()
	{
		setup();
		list = hib.sort(list, TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[i]);
		}
	}
	
	public void testInsertionSortDescendente()
	{
		setup();
		list = hib.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[guia.length - 1 - i]);
		}
	}
}
