package taller.mundo.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.InsertionSortTeam;
import junit.framework.TestCase;

public class InsertionSortTeamTest extends TestCase {
	
	private InsertionSortTeam ins;
	private Comparable[] guia;
	private Comparable[] list;
	
	public void setup()
	{
		ins =  new InsertionSortTeam();
		guia = new Comparable[]{14,27,34,38,47,57,69,78,85,95};
		list = new Comparable[]{38,34,85,27,47,78,95,57,69,14};
	}
	
	public void testInsertionSortAscendente()
	{
		setup();
		list = ins.sort(list, TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[i]);
		}
	}
	
	public void testInsertionSortDescendente()
	{
		setup();
		list = ins.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[guia.length - 1 - i]);
		}
	}
}
