package taller.mundo.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.MergeSortTeam;
import junit.framework.TestCase;

public class MergeSortTeamTest extends TestCase{
	
	private	MergeSortTeam	mer;
	private	Comparable[]	guia;
	private	Comparable[]	list;
	
	protected void setup()
	{
		mer = new MergeSortTeam();
		guia = new Comparable[]{2,17,20,32,48,51,68,72,83,91};
		list = new Comparable[]{68,83,2,72,17,91,20,48,32,51};
	}
	
	public void testMergeSortAscendente()
	{
		setup();
		list =  mer.sort(list, TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[i]);
		}
	}
	
	
	public void testMergeSortDescendente()
	{
		setup();
		list =  mer.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i], guia[list.length - 1 - i]);
		}
	}

}
