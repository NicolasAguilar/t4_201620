package taller.mundo.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.QuickSortTeam;
import junit.framework.TestCase;

public class QuickSortTeamTest extends TestCase{
	
	private QuickSortTeam quick;
	private Comparable[] guia;
	private Comparable[] list;
	
	protected void setup()
	{
		quick = new QuickSortTeam();
		guia =  new Comparable[]{3,12,24,36,42,51,63,71,84,96};
		list =  new Comparable[]{84,42,12,36,3,96,71,24,51,63};
	}
	
	public void testQuickSortAscendente()
	{
		setup();
		list = quick.sort(list,TipoOrdenamiento.ASCENDENTE);
		for(int i = 0; i < list.length; i++)
		{
			assertEquals(list[i],guia[i]);
		}
	}
	
	public void testQuickSortDescendente()
	{
		setup();
		list = quick.sort(list, TipoOrdenamiento.DESCENDENTE);
		for(int i = 0; i < list.length;i++)
		{
			assertEquals(list[i], guia[guia.length - 1 - i]);
		}
	}
}
