package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista,0,lista.length-1, orden);
	}


	private static Comparable[] merge_sort(Comparable[] lista,int ini,int fin, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		if (fin <= ini) return lista;
		int mid = ini + (fin - ini)/2;
		merge_sort(lista, ini, mid, orden); // Sort left half.
		merge_sort(lista, mid+1, fin, orden); // Sort right half.
		merge(lista, ini, mid, fin, orden); // Merge results (code on page 271).
		return lista;
	}

	private static void merge(Comparable[] lista, int izquierda, int medio, int derecha, TipoOrdenamiento orden)
	{
		// Trabajo en Clase 
		int i = izquierda;
		int j = medio+1;
		Comparable[] aux = new Comparable[lista.length];
		
		for(int k = izquierda; k<=derecha;k++)
		{
			aux[k] = lista[k];
		}
		
		for(int k = izquierda; k <= derecha; k++)
			if(i > medio)
				lista[k] = aux[j++];
			else if (j > derecha)
				lista[k] = aux[i++];
			else if (aux[j].compareTo(aux[i]) < 0 && orden == TipoOrdenamiento.ASCENDENTE)
				lista[k] = aux[j++];
			else if (aux[j].compareTo(aux[i]) > 0 && orden == TipoOrdenamiento.DESCENDENTE)
				lista[k] = aux[j++];
			else
				lista[k] = aux[i++];
	}


}
