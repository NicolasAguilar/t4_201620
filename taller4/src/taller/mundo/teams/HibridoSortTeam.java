package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class HibridoSortTeam extends AlgorithmTeam{

	//MERGE Y BUBBLE SORT
	private MergeSortTeam mer;

	public HibridoSortTeam() {
		super("Hibrido sort (*)");
		mer = new MergeSortTeam();
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden) {
		return hibridoSort(list, orden);
	}

	public Comparable[] hibridoSort(Comparable[] lis, TipoOrdenamiento orden)
	{
		boolean cambio = true;
		int j = 1;
		while(j>lis.length/2)
		{
			cambio = false;
			for(int i = 0; i < lis.length-j; i++)
			{
				if(lis[i].compareTo(lis[i+1]) > 0 && orden == TipoOrdenamiento.ASCENDENTE)
				{
					Comparable temp = lis[i];
					lis[i] = lis[i+1];
					lis[i+1] = temp;
					cambio = true;
				}
				
				else if(lis[i].compareTo(lis[i+1]) < 0 && orden == TipoOrdenamiento.DESCENDENTE)
				{
					Comparable temp = lis[i];
					lis[i] = lis[i+1];
					lis[i+1] = temp;
					cambio = true;
				}
			}
			j++;
		}
		return mer.sort(lis, orden);
	}

}
